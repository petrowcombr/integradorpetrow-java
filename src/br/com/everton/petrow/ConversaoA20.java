package br.com.everton.petrow;

import java.net.URLEncoder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

public class ConversaoA20 {

    private static String erro;

    public static String getErro() {
        return erro;
    }

    public static double[] converter(String url, String locale, String appCode, String token, String kind, double temperature, double density, double volume) throws Exception {
        // converte e retorna um array(density_20, correction_factor, volume_20, gross, degree_inpm, degree_gl)
        erro = "";
        
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url + "/?locale=" + locale + "&appCode=" + URLEncoder.encode(appCode, "UTF-8") + "&token=" + token + "&kind=" + kind + "&temperature=" + temperature + "&density=" + density + "&volume=" + volume)
                .addHeader("cache-control", "no-cache")
                .build();

        Response response;
        
        response = client.newCall(request).execute();

        if (response.code() == 200) {
            JSONObject json = new JSONObject(response.body().string().trim());

            if (json.getString("status").equals("OK")) {
                response.close();
                
                if (kind.equalsIgnoreCase("P")) {
                    // derivado de petróleo
                    return new double[]{json.getDouble("density_20"), json.getDouble("correction_factor"), json.getDouble("volume_20"), json.getDouble("gross"), 0, 0};
                } else {
                    // etanol
                    return new double[]{json.getDouble("density_20"), json.getDouble("correction_factor"), json.getDouble("volume_20"), json.getDouble("gross"), json.getDouble("degree_inpm"), json.getDouble("degree_gl")};
                }
            } else {
                // erro
                for (int i = 0; i < json.getJSONObject("errors").names().length(); i++) {
                    erro += "[" + json.getJSONObject("errors").names().getString(i) + "] " + json.getJSONObject("errors").get(json.getJSONObject("errors").names().getString(i)) + "\n";
                }
            }
        } else {
            erro += "[" + response.code() + "] " + response.message() + " - " + url;
        }
        
        response.close();

        return new double[]{0, 0, 0, 0, 0, 0};
    }
}
