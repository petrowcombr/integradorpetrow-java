package br.com.everton.petrow;

import java.net.URLEncoder;
import java.util.Map;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Consumir {

    public static Response getResponse(String url, String appCode, String token, String locale, Map<String, String> parametros) throws Exception {
        StringBuilder requisicao = new StringBuilder();

        requisicao.append(url);
        requisicao.append("/?appCode=").append(URLEncoder.encode(appCode, "UTF-8"));
        requisicao.append("&token=").append(URLEncoder.encode(token, "UTF-8"));
        requisicao.append("&locale=").append(URLEncoder.encode(locale, "UTF-8"));

        for (String key : parametros.keySet()) {
            requisicao.append("&").append(key).append("=").append(URLEncoder.encode(parametros.get(key).trim(), "UTF-8"));
        }
        
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(requisicao.toString())
                .addHeader("cache-control", "no-cache")
                .build();

        return client.newCall(request).execute();
    }
}
