package br.com.everton.petrow;

import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class Main {

    /*
    ATENÇÃO! É necessário importar o certificado SSL da página https://www.petrow.com.br 
    para que você consiga se comunicar com o nosso servidor através de uma aplicação Java WEB
    Baixe o certificado gerando o arquivo ".cer" e importe o mesmo usando o comando abaixo:
    keytool -import -alias petrow.com.br -file petrowcombr.cer -keystore cacerts
     */
    public static void main(String[] args) {
        // exemplo de uso de algumas das soluções - teste
        try {
            String appCode = "298dc19d489c4e4db73e28a713eab07b"; // informe o seu APP Code
            String appSecret = "379aa06aaf7f71317d89a464d8ac30b5"; // informe o seu APP Secret
            String locale = "pt"; // informe o seu locale (pt / es / en)

            // token
            String token = Token.gerar("https://www.petrow.com.br/api/v1/tokens", locale, appCode, appSecret);
            System.out.println("TOKEN GERADO: " + token + "\n");

            if (token != null) {
                // conversão a 20 - derivado de petróleo
                //TestConversaoA20Petroleo(appCode, token);

                // conversão a 20 - etanol
                //TestConversaoA20Etanol(appCode, token);
                // perfil do agente (raíz do cnpj)
                TestPerfilDoAgenteRaizCnpj(appCode, token);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e.getMessage());
        }
    }

    public static void TestConversaoA20Petroleo(String appCode, String token) throws Exception {
        double temperatura_petroleo = 22;
        double densidade_petroleo = 0.897;
        double volume_petroleo = 15000;
        double[] conversaoa20_petroleo;

        conversaoa20_petroleo = ConversaoA20.converter("https://www.petrow.com.br/api/v1/conversions/to20", "pt", appCode, token, "P", temperatura_petroleo, densidade_petroleo, volume_petroleo);

        System.out.println("CONVERTER DERIVADO DE PETRÓLEO");
        System.out.println("-- temperatura (°C)\t\t" + temperatura_petroleo);
        System.out.println("-- densidade (kg/L)\t\t" + densidade_petroleo);
        System.out.println("-- volume (L)\t\t\t" + volume_petroleo);
        System.out.println("-- densidade a 20°C (kg/L)\t" + conversaoa20_petroleo[0]);
        System.out.println("-- fator de correção\t\t" + conversaoa20_petroleo[1]);
        System.out.println("-- volume a 20°C (L)\t\t" + conversaoa20_petroleo[2]);
        System.out.println("-- massa (kg)\t\t\t" + conversaoa20_petroleo[3]);
        System.out.println();
    }

    public static void TestConversaoA20Etanol(String appCode, String token) throws Exception {
        double temperatura_etanol = 27;
        double densidade_etanol = 0.888;
        double volume_etanol = 5000;
        double[] conversaoa20_etanol;

        conversaoa20_etanol = ConversaoA20.converter("https://www.petrow.com.br/api/v1/conversions/to20", "pt", appCode, token, "E", temperatura_etanol, densidade_etanol, volume_etanol);

        System.out.println("CONVERTER ETANOL");
        System.out.println("-- temperatura (°C)\t\t" + temperatura_etanol);
        System.out.println("-- densidade (kg/L)\t\t" + densidade_etanol);
        System.out.println("-- volume (L)\t\t\t" + volume_etanol);
        System.out.println("-- densidade a 20°C (kg/L)\t" + conversaoa20_etanol[0]);
        System.out.println("-- fator de correção\t\t" + conversaoa20_etanol[1]);
        System.out.println("-- volume a 20°C (L)\t\t" + conversaoa20_etanol[2]);
        System.out.println("-- massa (kg)\t\t\t" + conversaoa20_etanol[3]);
        System.out.println("-- INPM (°)\t\t\t" + conversaoa20_etanol[4]);
        System.out.println("-- GL (°)\t\t\t" + conversaoa20_etanol[5]);
        System.out.println();
    }

    public static void TestPerfilDoAgenteRaizCnpj(String appCode, String token) throws Exception {
        System.out.println("++ Perfil do agente (Raíz do CNPJ)");

        String url = "https://www.petrow.com.br/api/v1/datasets/anp/simp/t001/PerfilDoAgente";
        String locale = "pt";
        String raizCnpj = "34274233";

        Response response = null;

        try {
            response = PerfilDoAgente.getRaizCnpj(url, appCode, token, locale, raizCnpj);

            JSONObject json = new JSONObject(response.body().string().trim());

            if (json.getString("status").equals("OK")) {

                JSONArray resultados = new JSONArray(json.getString("result"));

                for (int i = 0; i < resultados.length(); i++) {
                    JSONObject resultado = new JSONObject(resultados.getString(i));

                    System.out.println("-- Raíz do CNPJ\t\t" + resultado.getString("raiz_cnpj"));
                    System.out.println("-- Razão Social\t\t" + resultado.getString("razao_social"));
                    System.out.println("-- Código ANP\t\t" + resultado.getString("cod_agente_anp"));

                    // tipos
                    JSONArray tipos = new JSONArray(resultado.getString("tipos"));

                    if (tipos.length() > 0) {
                        System.out.println("-- Tipo");
                        
                        for (int i2 = 0; i2 < tipos.length(); i2++) {
                            System.out.println("\t" + tipos.get(i2));
                        }
                    }

                    System.out.println("-- CEP\t\t\t" + resultado.getString("cep"));
                    System.out.println("-- Endereço\t\t" + resultado.getString("endereco"));
                    System.out.println("-- Bairro\t\t" + resultado.getString("bairro"));
                    System.out.println("-- Município\t\t" + resultado.getString("municipio"));
                    System.out.println("-- Estado\t\t" + resultado.getString("estado"));
                    System.out.println("-- Início da Validade\t" + resultado.getString("data_inicio_validade"));
                    System.out.println("-- Final da Validade\t" + resultado.getString("data_final_validade"));
                    System.out.println();
                }
            } else {
                // errors
                for (int i = 0; i < json.getJSONObject("errors").names().length(); i++) {
                    System.out.println("[" + json.getJSONObject("errors").names().getString(i) + "] " + json.getJSONObject("errors").get(json.getJSONObject("errors").names().getString(i)));
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());

            if (response != null) {
                System.out.println("[" + response.code() + "] " + response.message() + " - " + url);
            }
        }
    }
}
