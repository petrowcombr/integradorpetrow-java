package br.com.everton.petrow;

import java.net.URLEncoder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;

public class Token {

    private static String erro;

    public static String getErro() {
        return erro;
    }

    public static String gerar(String url, String locale, String appCode, String appSecret) throws Exception {
        // gera e retorna uma string contendo o token
        erro = "";

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url + "/?locale=" + locale + "&appCode=" + URLEncoder.encode(appCode, "UTF-8") + "&appSecret=" + appSecret)
                .addHeader("cache-control", "no-cache")
                .build();
        
        Response response;

        response = client.newCall(request).execute();

        if (response.code() == 200) {
            JSONObject json = new JSONObject(response.body().string().trim());

            if (json.getString("status").equals("OK")) {
                response.close();
                
                return json.getString("token");
            } else {
                // erro
                for (int i = 0; i < json.getJSONObject("errors").names().length(); i++) {
                    erro += "[" + json.getJSONObject("errors").names().getString(i) + "] " + json.getJSONObject("errors").get(json.getJSONObject("errors").names().getString(i)) + "\n";
                }
            }
        } else {
            erro += "[" + response.code() + "] " + response.message() + " - " + url;
        }

        response.close();
        
        return null;
    }
}