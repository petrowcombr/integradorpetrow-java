package br.com.everton.petrow;

import java.net.URLEncoder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PerfilDoAgente {

    public static Response getRaizCnpj(String url, String appCode, String token, String locale, String raizCnpj) throws Exception {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url +  "/?appCode=" + URLEncoder.encode(appCode, "UTF-8") + "&token=" + URLEncoder.encode(token, "UTF-8") + "&locale=" + URLEncoder.encode(locale, "UTF-8") + "&raizCnpj=" + URLEncoder.encode(raizCnpj, "UTF-8"))
                .build();

        return client.newCall(request).execute();
    }
}
